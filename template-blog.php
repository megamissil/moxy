<?php
/**
 * Template Name: Blog Template
 */

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );
$url = $thumb['0'];

get_header(); ?>

<?php if ( has_post_thumbnail() ) { ?>
<div class="blog-title" style="background-image: url(<?=$url; ?>)">
<?php } else { ?>
<div class="blog-title" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-bg-blog.jpg)">
<?php } ?>

	<p><?php the_title(); ?></p>
</div>

<section class="blog-container">
	<div class="row">
		<div class="large-8 medium-9 small-11 small-centered columns" role="main">

		<!-- Start the Loop. -->
		<?php query_posts('cat=blog'); ?>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
			<div class="blog-summary">
				<h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

				<small><?php the_time('F jS, Y'); ?> by <?php the_author_posts_link(); ?></small>

				<?php the_post_thumbnail(); ?>

				<?php the_excerpt(); ?>
				</div><!-- /.blog-summary -->

			<hr>

		<?php endwhile; ?>

			<div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
			<div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>

		<?php else : ?>

			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

		<?php endif; ?>
		<?php wp_reset_query(); ?>
		
		<div class="clearfix"></div>

		</div>
	</div><!-- /.row -->
</section><!-- /.single-container -->

<?php get_footer(); ?>
