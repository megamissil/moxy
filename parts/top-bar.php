<?php
/**
 * Template part for top bar menu
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<div class="top-bar-container main-nav contain-to-grid show-for-medium-up">
    <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
            <li class="name">
                <?php /* <h1><a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a></h1> */ ?>
            </li>
        </ul>
        <section class="top-bar-section">
            <?php if (is_page('talent')) { ?>

                <?php foundationpress_top_bar_talent(); ?>

            <?php } elseif (is_page('creative')) { ?>

                <?php foundationpress_top_bar_creative(); ?>

            <?php } else { ?>

                <?php foundationpress_top_bar_l(); ?>
                <?php foundationpress_top_bar_r(); ?>

            <?php } ?>
        </section>
    </nav>
</div>