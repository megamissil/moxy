<?php
/**
 * Template Name: Creative Template
 */

get_header(); ?>

<div class="banner-divider creative-tagline hide-for-small" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-creative-tagline-bg.jpg">
	<p>Let's make you<br />
	<strong>impossible to ignore.</strong></p>
</div>

<!-- PORTFOLIO -->
<section class="portfolio">
	<div class="anchor" id="portfolio"></div>
	<div class="row">
		<div class="large-12 small-11 small-centered columns">
			<h2 class="text-center">Portfolio</h2>

			<div class="row">
				<div class="large-4 medium-5 small-8 small-centered columns">
					<hr class="title-underline">
				</div>
			</div>
			
			<div class="portfolio-slider">
				<div> <!-- Slide -->
					<div class="row">
						<?php
						    $args = array(
						      	'post_type' => 'portfolio',
						      	'showposts' => -1
						    );
						    $members = new WP_Query( $args );
						    if( $members->have_posts() ) {
						    	$i = 0;
								while( $members->have_posts() ) {
									$members->the_post();
									?>
										
										<div class="large-2 medium-3 small-6 columns left col-height portfolio-item" id="<?=$i; ?>">
											<a href="#" data-reveal-id="<?=$post->post_name; ?>">
											<?php /* <a data-remodal-target="<?=$post->post_name; ?>"> */ ?>
												<?php the_post_thumbnail( 'thumbnail' ); ?>
											</a>
										</div>
									
										<?php if (($i >= 11) && ($i % 11 == 0)): ?>
											
											</div><!-- /.row -->
										</div><!-- END slide -->
										<div><!-- Open slide -->
											<div class="row">

										<?php endif ?>

										<?php if ($i == 11) {

											$i++;
											$i = 0;

										} else {

											$i++;
											
										} ?>
								  
									<?php
								}
						    }
						    else {
						      	echo 'Portfolio is coming soon!';
						    }
						?>
					</div><!-- /.row -->
				</div><!-- END slide -->
			</div><!-- /.portfolio-slider -->

			<?php
			    $args = array(
			      	'post_type' => 'portfolio',
			      	'showposts' => -1
			    );
			    $members = new WP_Query( $args );
			    if( $members->have_posts() ) {
					while( $members->have_posts() ) {
						$members->the_post();
						?>
							
							<div id="<?=$post->post_name; ?>" class="reveal-modal portfolio-detail" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">

							<?php /* <div class="remodal portfolio-detail" data-remodal-id="<?=$post->post_name; ?>" data-remodal-options="hashTracking: false">
							  	<button data-remodal-action="close" class="remodal-close"></button> */ ?>
								
								<div class="row">
									<div class="large-9 small-12 small-centered columns">
										<h2 class="text-center">
											<?php if (types_render_field('portfolio-website', array('raw' => 'true'))) { ?>

												<a href="<?= types_render_field('portfolio-website', array('output' => 'raw')); ?>" target="_new">
													<?php the_title(); ?> <i class="fa fa-link"></i>
												</a>
											
											<?php } else { ?>
											
												<?php the_title(); ?>
											
											<?php } ?>
										</h2>
										
										<?php 
											$posttags = get_the_tags();
										    if ($posttags) {
										       	$taglist = "";
										       	foreach($posttags as $tag) {
										           	$taglist .=  $tag->name . ' // '; 
										       	}
										       	echo "<p class='portfolio-type'>";
										      	echo rtrim($taglist, " // ");
										      	echo "</p>";
											} 
										?>
										
										<div class="portfolio-detail-slider">
											<div>
											<?php echo types_render_field('portfolio-images', array('separator' => '</div><div>')); ?>
											</div>
										</div>

										<div class="portfolio-detail-slider-nav">
											<div>
											<?php echo types_render_field('portfolio-images', array('size' => 'thumbnail', 'separator' => '</div><div>')); ?>
											</div>
										</div>

										<?php the_content(); ?>
									</div>
								</div>

							  	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
							</div>
					  
						<?php
					}
			    }
			    else {
			      	echo 'Portfolio is coming soon!';
			    }
			?>

			
		</div>
	</div>
</section>
<!-- END // PORTFOLIO -->

<div class="banner-divider hide-for-small" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-new-bg-poster.jpg"></div>

<!-- SERVICES -->
<section class="services">
	<div class="anchor" id="services"></div>
	<div class="row fullWidth">
		<div class="large-11 medium-10 small-11 small-centered columns">
			<h2 class="text-center">Services</h2>

			<div class="row">
				<div class="large-4 medium-5 small-8 small-centered columns">
					<hr class="title-underline">
				</div>
			</div>
			
			<!-- DESKTOP -->
			<div class="row show-for-large-up">
				<div class="large-2 medium-3 small-6 columns col-height"> <!-- Opens first column -->
				<?php
				    $args = array(
				      	'post_type' => 'service',
				    );
				    $members = new WP_Query( $args );
				    if( $members->have_posts() ) {
				    	$i = 0;
						while( $members->have_posts() ) {
							$members->the_post();
							?>

								<?php if ($i % 3 == 2): ?>
									
									</div> <!-- Closes column before third iteration -->

									<div class="large-2 columns col-height third">
										<div class="service element">
											<?php the_post_thumbnail(); ?>
											<h3><?php the_title(); ?></h3>
											<?php the_content(); ?>
										</div>
									</div>

									<?php if ($i != 8): ?>
										<div class="large-2 columns col-height"> <!-- Prevents stray open container on last iteration -->
									<?php endif ?>
									
								<?php else: ?>
									
										<div class="service">
											<?php the_post_thumbnail(); ?>
											<h3><?php the_title(); ?></h3>
											<?php the_content(); ?>
										</div>

								<?php endif ?>									
						  
							<?php
							$i++;
						}
				    }
				    else {
				      	echo 'Services have not been added yet';
				    }
				?>
			</div><!-- /.row -->
			<!-- END // DESKTOP -->

			<!-- TABLET / MOBILE -->
			<div class="row show-for-medium-down">
				<?php
				    $args = array(
				      	'post_type' => 'service',
				    );
				    $members = new WP_Query( $args );
				    if( $members->have_posts() ) {
				    	$i = 0;
						while( $members->have_posts() ) {
							$members->the_post();
							?>
								
								<div class="medium-4 small-6 columns left col-height">
									<div class="service">
										<?php the_post_thumbnail(); ?>
										<h3><?php the_title(); ?></h3>
										<?php the_content(); ?>
									</div>
								</div>
						  
							<?php
							$i++;
						}
				    }
				    else {
				      	echo 'Services have not been added yet';
				    }
				?>
			</div><!-- /.row -->
			<!-- END // TABLET / MOBILE -->

		</div>
	</div>
</section>
<!-- END // SERVICES -->

<!-- LET'S GET STARTED -->
<section class="lets-get-started">
	<div class="anchor" id="lets-get-started"></div>
	<div class="row">
		<div class="small-11 small-centered columns">
			<h2 class="text-center">Let's Get Started</h2>
			<div class="row">
				<div class="large-4 medium-5 small-8 small-centered columns">
					<hr class="title-underline">
				</div>
			</div>

			<div class="row">
				<div class="medium-10 medium-centered columns">
					<p>Stoked about what you see?  We can help you tap into your creative mojo - regardless of budget.</p>
					<p>Fill out the questionnaire below and we’ll holla back. </p>
				</div>
				<div class="large-5 medium-6 medium-centered columns">
					<a href="<?php echo site_url(); ?>/creative-questionnaire" class="button">Creative Questionnaire</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- END // LET's GET STARTED -->

<div class="banner-divider hide-for-small" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-new-bg-signangle.jpg"></div>

<?php get_footer(); ?>