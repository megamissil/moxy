<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div class="banner-divider hide-for-small" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bg-make-change-be.jpg"></div>

<!-- OUT STORY -->
<section class="about-us">
	<div class="anchor" id="our-story"></div>
	<div class="row">
		<div class="small-11 small-centered columns">
			<h1>Our Story</h1>

			<div class="row">
				<div class="large-4 medium-5 small-8 small-centered columns">
					<hr class="title-underline">
				</div>
			</div>
			
			<div class="row">
				<div class="medium-9 small-10 small-centered columns">
					<div class="row">
						<div class="medium-6 columns">
							<a data-remodal-target="why-we-do">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-whole-team.jpg" alt="">
							</a>
							<a data-remodal-target="why-we-do" class="btn-why">Talent and Creative</a>
						</div>
						<div class="medium-6 columns">
							<a data-remodal-target="the-moxy-story" class="btn-moxy-story hide-for-small">The Moxy Story</a>
							<a data-remodal-target="the-moxy-story">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-bk.jpg" alt="">
							</a>
							<a data-remodal-target="the-moxy-story" class="btn-moxy-story show-for-small">The Moxy Story</a>
						</div>
					</div><!-- /.row -->
				</div>
			</div><!-- /.row -->
			
			<!-- WHY WE DO BOTH -->
			<div class="remodal our-story-modal" data-remodal-id="why-we-do" data-remodal-options="hashTracking: false">
				<button data-remodal-action="close" class="remodal-close"></button>
				
				<h3>Why We Do Both</h3>
				<p>(Mok- see): a slang term for daring, bold, fearless.</p>
				<p>Moxy isn’t your typical staffing or creative firm.  In fact, we are neither one nor the other, we are both.  Our Creative Agency  designs, brands, builds and showcases talent and companies in an innovative way.  Our Talent Agency recruits and places passive talent with start-ups, Fortune 500 companies and anything in between.  We take a fearless approach to staffing and a bold approach to creativity.  We just do things differently!  You dig?</p>
			</div>

			<!-- THE MOXY STORY -->
			<div class="remodal our-story-modal" data-remodal-id="the-moxy-story" data-remodal-options="hashTracking: false">
				<button data-remodal-action="close" class="remodal-close"></button>

				<h3>The Moxy Story</h3>
				<!-- Pulls content from Our Story page -->
				<?php 
				  $slug = get_page_by_path('our-story',OBJECT,'page');
				  $post = get_post($slug);
				  $title = apply_filters('the_title', $post->post_title);
				  $content = apply_filters('the_content', $post->post_content); 
				?>

				<?php echo $content; ?>
			</div>

		</div>
	</div><!-- /.row -->
</section><!-- /.about-us -->

<div class="banner-divider hide-for-small" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-new-bg-signhome.jpg"></div>

<!-- MEET THE TEAM -->
<section class="meet-the-team">
	<div class="anchor" id="meet-the-team"></div>
	<div class="row">
		<div class="large-11 medium-9 medium-centered columns">
			<h2>Meet the Team</h2>

			<div class="row">
				<div class="large-4 medium-5 small-8 small-centered columns">
					<hr class="title-underline">
				</div>
			</div>
			
			<!-- DESKTOP/TABLET: The Team -->
			<div class="team-carousel hide-for-small">
				
				<!-- Team Member Loop -->
				<?php
				    $args = array(
				      	'post_type' => 'team-member',
				      	'showposts' => -1
				    );
				    $members = new WP_Query( $args );
				    if( $members->have_posts() ) {
						while( $members->have_posts() ) {
							$members->the_post();
							?>

								<div>
									<a data-remodal-target="<?php the_title(); ?>">
										<?php the_post_thumbnail(); ?>
									</a>
								</div>

								<?php /*
								<div>
									<a href="#" data-reveal-id="<?php the_title(); ?>">
										<?php the_post_thumbnail(); ?>
									</a>
								</div>
								*/ ?>
						  
							<?php
						}
				    }
				    else {
				      	echo 'Members are currently being edited';
				    }
				?>
				<!-- END Team Member Loop -->
			</div><!-- /.team-carousel -->

			<!-- Member Details -->
			<?php
			    $args = array(
			      	'post_type' => 'team-member',
				    'showposts' => -1
			    );
			    $members = new WP_Query( $args );
			    if( $members->have_posts() ) {
					while( $members->have_posts() ) {
						$members->the_post();
						?>
							
							<div class="remodal member-detail" data-remodal-id="<?php the_title(); ?>" data-remodal-options="hashTracking: false">
							  	<button data-remodal-action="close" class="remodal-close"></button>
							  	
							  	<div class="row">
									<div class="small-12 columns">
										<h2 class="text-center">
											<?=types_render_field( 'member-first-name' ); ?> <?=types_render_field( 'member-last-name' ); ?> 
											<span><?=types_render_field( 'member-job-title' ); ?></span>
										</h2>
									</div>
									<div class="medium-3 medium-uncentered small-8 small-centered columns member-thumb">
										<?php the_post_thumbnail(); ?>
									</div>
									<div class="medium-9 columns">
										
										<p class="one-liner">"<?=types_render_field( 'member-one-liner', array('output' => 'raw') ); ?>"</p>
							  			
							  			<?php the_content(); ?>

							  			<a href="<?=types_render_field( 'member-facebook-url', array('output' => 'raw') ); ?>" target="_blank">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/icon-facebook-purple.png" alt="">
										</a>

										<a href="http://twitter.com/<?=types_render_field( 'member-twitter-username' ); ?>" target="_blank">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/icon-twitter-purple.png" alt="">
										</a>

										<a href="<?=types_render_field( 'member-linkedin-url', array('output' => 'raw') ); ?>" target="_blank">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/icon-linkedin-purple.png" alt="">
										</a>

							  			<a href="mailto:<?=types_render_field( 'member-email', array('output' => 'raw') ); ?>">
						  					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/icon-mail-purple.png" alt="">
						  				</a>

									</div>
								</div><!-- /.row -->
							</div><!-- /.remodal -->
					
						<?php
					}
			    }
			    else {
			      	echo 'Members are currently being edited';
			    }
			?>
			<!-- END Member Details -->
			
			<!-- MOBILE: The Team -->
			<div class="row show-for-small the-team-mobile">
				<div class="small-11 small-centered columns">
					<div class="row">

						<!-- Team Member Loop -->
							<?php
						    $args = array(
						      	'post_type' => 'team-member',
				      			'showposts' => -1
						    );
						    $members = new WP_Query( $args );
						    if( $members->have_posts() ) {
								while( $members->have_posts() ) {
									$members->the_post();
									?>
										
										<div class="small-6 columns">
											<a data-remodal-target="<?php the_title(); ?>">
												<?php the_post_thumbnail(); ?>
											</a>
										</div>
								  
									<?php
								}
						    }
						    else {
						      	echo 'Members are currently being edited';
						    }
						?>
						<!-- END Team Member Loop -->

					</div><!-- /.row -->
				</div>
			</div>
			<!-- END -->
			
		</div>
	</div>
</section>

<div class="banner-divider hide-for-small" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bg-moxy-building.jpg"></div>

<!-- MOXY BLOG -->
<section class="moxy-blog">
	<div class="anchor" id="blog"></div>
	<div class="row">
		<div class="medium-12 columns">
			<h2>Moxy Blog</h2>

			<div class="row">
				<div class="large-4 medium-5 small-8 small-centered columns">
					<hr class="title-underline">
				</div>
			</div>

			<div class="row blog-posts">
				
				<?php $counter = 1 ?>
				<?php query_posts('cat=blog&showposts=3'); ?>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				   
				   	<div class="large-4 medium-6 columns <?php if ($counter % 4 >= 3) { echo 'hide-for-medium-down'; } ?>">
						<div class="post col-height">
							<h3><?php the_title(); ?></h3>
							<hr>

							<?php the_excerpt(); ?>
							<a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
							<div class="clearfix"></div>
						</div>
					</div>
				
				<?php $counter++ ;
				endwhile; endif; ?>
				<?php wp_reset_query(); ?>

				<div class="small-12 columns text-center">
					<a href="<?php echo site_url(); ?>/blog" class="button">View Full Blog</a>
				</div>
			</div><!-- /.row -->
		</div>
	</div> <!-- /.row -->
</section>

<div class="banner-divider hide-for-small" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bg-lobby-chairs.jpg"></div>

<!-- PARTNERS -->
<section class="partners">
	<div class="anchor" id="partners"></div>
	<div class="row">
		<div class="large-12 medium-10 small-11 small-centered columns">
			<h2>Partners</h2>

			<div class="row">
				<div class="large-4 medium-6 small-8 small-centered columns">
					<hr class="title-underline">
				</div>
			</div>

			<div class="row text-center partner-logos">
				<div class="medium-4 small-6 columns col-height logo">
					<a href="http://techontapbham.com/" target="_new">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partners/Tech-on-Tap-logo1.png" alt="" height="100">
					</a>
				</div>
				<div class="medium-4 small-6 columns col-height logo">
					<a href="https://www.techbirmingham.com/" target="_new">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partners/Tech_Birmingham_2015.png" alt="">
					</a>
				</div>
				<div class="medium-4 small-6 columns col-height logo left">
					<p class="show-for-small">&nbsp;</p>
					<a href="http://birminghambusinessalliance.com/" target="_new">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partners/bba.jpg" alt="">
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>