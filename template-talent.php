<?php
/**
 * Template Name: Talent Template
 */

get_header(); ?>

<div class="banner-divider talent-tagline hide-for-small" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-talent-tagline-bg.jpg">
	<p>Staffing.<br />
	<strong>On Steroids.</strong></p>
</div>

<!-- MOXY JOBS -->
<section class="moxy-jobs">
	<div class="anchor" id="moxy-jobs"></div>
	<div class="row">
		<div class="small-11 small-centered columns">
			<h2 class="text-center">Moxy Jobs</h2>
			
			<div class="row">
				<div class="large-4 medium-5 small-8 small-centered columns">
					<hr class="title-underline">
				</div>
			</div>

			<div class="row">
				<div class="large-8 medium-7 columns">
					<script src="https://app.jobcast.net/widget/19329-58270/jobs"></script>
				</div>
				<div class="large-4 medium-5 columns hide-for-small">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/MOXY-dont-see-graphic.jpg" alt="">
					<a href="#join-talent-network" class="btn-join-network">
						Join the Moxy<br /> Talent Network!
						<hr>
						<div class="chevron-arrow"></div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- END // MOXY JOBS -->

<div class="banner-divider hide-for-small" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-new-bg-dig.jpg"></div>

<!-- HIRE MOXY TALENT -->
<section class="hire-moxy-talent">
	<div class="anchor" id="hire-talent"></div>
	<div class="row">
		<div class="large-10 small-11 small-centered columns">
			<h2 class="text-center">Hire Moxy Talent</h2>
			
			<div class="row">
				<div class="large-4 medium-5 small-8 small-centered columns">
					<hr class="title-underline">
				</div>
			</div>

			<div class="row purple-bg-form">
				<?php echo do_shortcode( '[contact-form-7 id="146" title="Hire Moxy Talent"]' ); ?>
			</div>
		</div>
	</div>
</section>
<!-- END // HIRE MOXY TALENT -->

<div class="banner-divider" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-new-bg-skyline.jpg"></div>

<!-- MOXY TALENT NETWORK -->
<section class="moxy-talent-network">
	<div class="anchor" id="join-talent-network"></div>
	<div class="row">
		<div class="large-10 small-11 small-centered columns">
			<h2 class="text-center">Join the Moxy Talent Network</h2>
			
			<div class="row">
				<div class="large-4 medium-5 small-8 small-centered columns">
					<hr class="title-underline">
				</div>
			</div>

			<div class="row purple-bg-form">
				<?php echo do_shortcode( '[contact-form-7 id="149" title="Join the Moxy Talent Network"]' ); ?>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</section>
<!-- END // MOXY TALENT NETWORK -->

<?php get_footer(); ?>