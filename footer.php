<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0.0
 */

?>

<!-- Contact Us -->
<section class="contact-us">
	<div class="anchor" id="contact-us"></div>
	<div class="row">
		<div class="medium-12 small-11 small-centered columns">
			<h2>Contact Us</h2>

			<div class="row">
				<div class="large-4 medium-5 small-8 small-centered columns">
					<hr class="title-underline">
				</div>
			</div>
			
			<div class="row">
				<div class="large-8 medium-9 medium-centered columns">
					<div class="row contact-form">
						
						<!-- Pulls content from Contact Us page -->
						<?php 
						  $slug = get_page_by_path('contact-us',OBJECT,'page');
						  $post = get_post($slug);
						  $title = apply_filters('the_title', $post->post_title);
						  $content = apply_filters('the_content', $post->post_content); 
						?>

						<?php echo $content; ?>

					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>

</section>
<footer>
	<div class="row">
		<div class="medium-6 columns copy">
			<p>
				<span>
					Copyright &copy; <?=date('Y'); ?> Moxy | 
				</span> 
				<span>
					<a href="https://www.facebook.com/moxyjobs/" target="_new"><i class="fa fa-facebook-square"></i></a>
				</span>
				<span>
					<a href="https://twitter.com/digmoxy" target="_new"><i class="fa fa-twitter-square"></i></a>
				</span>
				<span>
					<a href="https://www.linkedin.com/company/moxy-jobs?trk=top_nav_home" target="_new"><i class="fa fa-linkedin-square"></i></a>
				</span>
			</p>
		</div>
		<div class="medium-6 columns slug">
			<p>Website Designed and Built by <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-slug-white.png" alt=""></p>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

<script>
	// Intro Video Fadeout
	setTimeout(function() {
	    $('.intro-vid-container').fadeOut('slow');
	}, 4000); // <-- time in milliseconds
</script>

<script>
	// Animate.css
	$('#talentLink').addClass('animated pulse');
	$('#creativeLink').addClass('animated pulse');
</script>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/remodal/dist/remodal.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/matchHeight/jquery.matchHeight.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/vendor/parallax.min.js"></script>
<script>
	// Parallax
	$('.banner-divider').parallax({
	  speed: 0.7
	});
</script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/sticky/jquery.sticky.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/slick-carousel/slick/slick.min.js"></script>
<script>
	// Slick Slider
	$(document).ready(function(){
	  $('.homepage-hero-bgs').slick({
	    autoplay : true,
	    arrows   : false,
	    fade     : true
	  });
	  $('.talent-service-slider').slick({
	    autoplay      : true,
	    arrows        : false,
	    autoplaySpeed : 1500
	  });
	  $('.creative-service-slider').slick({
	    autoplay      : true,
	    arrows        : false,
	    autoplaySpeed : 1500
	  });
	  $('.portfolio-slider').slick({
	    autoplay : false,
	    arrows   : false,
	    dots     : true
	  });
	  $('.portfolio-detail-slider').slick({
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    arrows: false,
	    fade: true,
	    asNavFor: '.portfolio-detail-slider-nav'
	  });
	  $('.portfolio-detail-slider-nav').slick({
	    slidesToShow: 6,
	    slidesToScroll: 1,
	    asNavFor: '.portfolio-detail-slider',
	    dots: false,
	    arrows: false,
	    focusOnSelect: true
	  }); 
	});
</script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/owl-carousel-2/owl.carousel.min.js"></script>
<script>
	// Owl Carousel
	$(document).ready(function(){
	  $('.team-carousel').owlCarousel({
	    items              : 6,
	    loop               : true,
	    autoplay           : true,
	    autoplayTimeout    : 1500,
	    autoplayHoverPause : true,
	    nav                : true,
	    navText            : [],
	    responsiveClass    : true,
	      responsive:{
	          0:{
	              items: 3,
	              nav: false
	          },
	          600:{
	              items: 4,
	              slideBy: 4
	          },
	          1000:{
	              items: 6
	          }
	      }
	  });
	});
</script>

<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
