<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );
$url = $thumb['0'];

get_header(); ?>

<?php if ( has_post_thumbnail() ) { ?>
<div class="single-title" data-parallax="scroll" data-image-src="<?=$url; ?>">
<?php } else { ?>
<div class="single-title" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-talent-tagline-bg.jpg">
<?php } ?>

	<p><?php the_title(); ?></p>
</div>

<section class="single-container">
	<div class="row">
		<div class="large-8 medium-9 small-11 small-centered columns" role="main">

		<?php do_action( 'foundationpress_before_content' ); ?>

		<?php while ( have_posts() ) : the_post(); ?>
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<?php do_action( 'foundationpress_post_before_entry_content' ); ?>
				<div class="entry-content">

				<?php the_content(); ?>
				</div>
				<hr>
				<div class="row">
					<div class="medium-4 columns">
						<?php userphoto_the_author_photo(); ?>
					</div>
					<div class="medium-8 columns author-info">
						<h5><?php the_author_posts_link(); ?></h5>
						<p><em><?php the_author_meta('description'); ?></em></p>
						<?php $author = get_comment_author_email_link(); ?>
						<ul>
							<li><a href="https://twitter.com/<?php the_author_meta('twitter'); ?>" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
							<li><a href="<?php the_author_meta('facebook'); ?>" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
						</ul>
					</div>
				</div>
			</article>
		<?php endwhile;?>

		<?php do_action( 'foundationpress_after_content' ); ?>

		</div>
	</div>
</section>

<?php get_footer(); ?>