// Owl Carousel
$(document).ready(function(){
  $('.team-carousel').owlCarousel({
    items              : 6,
    loop               : true,
    autoplay           : true,
    autoplayTimeout    : 3000,
    autoplayHoverPause : true,
    nav                : true,
    navText            : [],
    responsiveClass    : true,
      responsive:{
          0:{
              items: 3,
              nav: false
          },
          600:{
              items: 4,
              slideBy: 4
          },
          1000:{
              items: 6
          }
      }
  });
});