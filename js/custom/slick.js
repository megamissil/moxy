// Slick Slider
$('.homepage-hero-bgs').slick({
  autoplay : true,
  arrows   : false,
  fade     : true
});
$('.talent-service-slider').slick({
  autoplay : true,
  arrows   : false
});
$('.creative-service-slider').slick({
  autoplay : true,
  arrows   : false
});
$('.portfolio-slider').slick({
  autoplay : false,
  arrows   : false,
  dots     : true
});
$('.portfolio-detail-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.portfolio-detail-slider-nav'
});
$('.portfolio-detail-slider-nav').slick({
  slidesToShow: 6,
  slidesToScroll: 1,
  asNavFor: '.portfolio-detail-slider',
  dots: false,
  arrows: false,
  focusOnSelect: true
}); 