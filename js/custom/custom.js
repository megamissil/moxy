// Smooth Scroll to Anchor
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});


// Back Button
function goBack() {
    window.history.back();
}


// matchHeight
$(function() {
    $('.col-height').matchHeight();
});


// Removes .expanded class
$('.top-bar-section ul li>a').click(function() {
   $('.mobile-nav .top-bar').removeClass('expanded');
});


// Sticky Top Bar
$(document).ready(function(){
  $(".main-nav").sticky({topSpacing:0});
});