<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<?php if (!isset($_COOKIE["notacarrot"])) {
		setcookie("notacarrot", "beentheredonethat", time()+1800);
} ?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/moxy-favicon.png" />
		<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/Moxy-apple-touch.png" />

		<!-- CSS Animate -->
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/animate.css/animate.min.css">
		<!-- END -->

		<!-- Remodal -->
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/remodal/dist/remodal.css">
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/remodal/dist/remodal-default-theme.css">
		<!-- END -->

		<!-- Google Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Arimo:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
		<!-- END -->

		<!-- Owl Carousel Styles -->
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/owl-carousel-2/assets/owl.carousel.css" />
		<!-- END -->

		<!-- Slick Carousel -->
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/slick-carousel/slick/slick.css"/>

		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/overrides.css"/>
		<!-- END -->
		
		<?php wp_head(); ?>

		<meta name="google-site-verification" content="rIV5kgz9qdbmC_1t45fMuaA3DMX49fFQK2WMPQ0pIrI" />
	</head>
	<body <?php body_class(); ?>>
	<?php do_action( 'foundationpress_after_body' ); ?>

	<?php if (!$_COOKIE["notacarrot"] == "beentheredonethat") { ?>
		<?php if (is_home()) { ?>
		<!-- INTRO VIDEO --> 
		<div class="intro-vid-container">
			<video autoplay loop poster="<?php echo get_stylesheet_directory_uri(); ?>/moxy-landing-placeholder.png" id="intro-vid" class="animated fadeIn">
				<source src="<?php echo get_stylesheet_directory_uri(); ?>/moxy-landing-logo-compressed.mp4" type="video/mp4">
			</video>
		</div>
		<!-- END -->
		<?php } ?>
	<?php } ?>
	
	<div class="show-for-small">
		<div class="top-bar-container mobile-nav">
		    <nav class="top-bar" data-topbar role="navigation">
		        <ul class="title-area">
		            <li class="name">
		                <?php /* <h1><a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a></h1> */ ?>
		            </li>
		            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		        </ul>
		        <section class="top-bar-section">
		            <?php if (is_page('talent')) { ?>

		                <?php foundationpress_top_bar_talent(); ?>

		            <?php } elseif (is_page('creative')) { ?>

		                <?php foundationpress_top_bar_creative(); ?>

		            <?php } else { ?>

		                <?php foundationpress_top_bar_l(); ?>
		                <?php foundationpress_top_bar_r(); ?>

		            <?php } ?>
		        </section>
		    </nav>
		</div>
	</div>
	
	<?php if (is_home()) { ?>
		
		<section class="homepage-hero">
			<div class="homepage-hero-bgs">
				<div class="bg-1"></div>
				<div class="bg-2"></div>
				<div class="bg-3"></div>
			</div>
			<div class="homepage-hero-text-container">
				<div class="homepage-hero-text">
					<div class="moxy-logo">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-translucent-logo.png" alt="">
					</div>
					<div class="feature-text">
						A<br class="show-for-small" /> 
							<a href="<?php echo site_url(); ?>/talent">
								<span id="talentLink">Talent</span>
							</a>
							&amp; 
							<a href="<?php echo site_url(); ?>/creative">
								<span id="creativeLink">Creative</span>
							</a>
							<br class="show-for-small" /> Agency
					</div>
					<div class="sub-text hide-for-small">
						We do staffing. We do creative.<br />
						Branding people and companies since 2011.
					</div>
				</div>
			</div>
		</section>

	<?php } elseif (is_page('talent')) { ?>

		<section class="talent-hero">
			<a href="/" class="button btn-main hide-for-small">Return to Main</a>
			<div class="hero-text">
				<p><span class="talent fade-in one">The Talent Agency</span></p>
				<hr class="fade-in two">
				<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/up-logo-orange.png" alt="" class="up-icon fade-in three"><br /> 
				<span class="your fade-in four">Your</span></p>

				<div class="talent-service-slider fade-in four">
					<div><span class="service">STAFF</span></div>
					<div><span class="service">Career</span></span></div>
					<div><span class="service">TEAM</span></div>
					<div><span class="service">PAYCHECK</span></div>
				</div>
				
			</div>
		</section>

	<?php } elseif (is_page('creative')) { ?>

		<section class="creative-hero">
			<a href="/" class="button btn-main hide-for-small">Return to Main</a>
			<div class="hero-text fade-in">
				<p><span class="creative fade-in one">The Creative Agency</span></p>
				<hr class="fade-in two">
				<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/up-logo-purple.png" alt="" class="up-icon fade-in three"><br /> 
				<span class="your fade-in four">Your</span></p>

				<div class="creative-service-slider fade-in four">
					<div><span class="service">BRAND</span></div>
					<div><span class="service">WEBSITE</span></span></div>
					<div><span class="service">IMAGE</span></div>
					<div><span class="service">BUSINESS</span></div>
				</div>
			</div>
		</section>

	<?php } ?>

	<?php if (is_single()) { ?>

		<div class="row back-bar">
			<div class="small-12 columns">
				<a href="/blog" class="button">< Back</a>
			</div>
		</div>

	<?php } elseif (!is_page(array('talent','creative')) && !is_home()) { ?>
		
		<div class="row back-bar">
			<div class="small-12 columns">
				<a href="/" class="button">< Back</a>
			</div>
		</div>

	<?php } else { ?>

		<?php get_template_part( 'parts/top-bar' ); ?>

	<?php } ?>

	<!-- <div class="clearfix"></div> -->

<section class="container" role="document">
	<?php do_action( 'foundationpress_after_header' ); ?>
