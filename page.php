<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );
$url = $thumb['0'];

get_header(); ?>

<?php if ( has_post_thumbnail() ) { ?>
<div class="single-title" data-parallax="scroll" data-image-src="<?=$url; ?>">
<?php } else { ?>
<div class="single-title" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-talent-tagline-bg.jpg">
<?php } ?>

	<p><?php the_title(); ?></p>
</div>

<section class="single-container">
	<div class="row">
		<div class="large-8 medium-9 small-11 small-centered columns" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?>

		<?php endwhile;?>
		
		<div class="clearfix"></div>

		</div>
	</div><!-- /.row -->
</section><!-- /.single-container -->

<?php get_footer(); ?>
