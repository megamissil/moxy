<?php
/**
 * Template Name: Blog Template
 */

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );
$url = $thumb['0'];

get_header(); ?>


<div class="blog-title" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/moxy-bg-blog.jpg)">
	<p><?php // the_title(); ?></p>
</div>

<section class="blog-container">
	<div class="row">
		<div class="large-8 medium-9 small-11 small-centered columns" role="main">

		<div class="row">
			<div class="medium-4 columns">
				<?php userphoto_the_author_photo(); ?>
			</div>
			<div class="medium-8 columns author-info">
				<h4><?php echo get_the_author(); ?></h4>
				<p><em><?php the_author_meta('description'); ?></em></p>
				<?php $author = get_comment_author_email_link(); ?>
				<ul>
					<li><a href="https://twitter.com/<?php the_author_meta('twitter'); ?>" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
					<li><a href="<?php the_author_meta('facebook'); ?>" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
				</ul>
			</div>
		</div>
		<hr>
		<!-- Start the Loop. -->
		<?php
		global $authordata, $post;
		$authors_posts = get_posts( array( 'author' => $authordata->ID, 'post__not_in' => array( $post->ID ), 'posts_per_page' => 5 ) ); ?>
		<?php foreach ( $authors_posts as $authors_post ) { ?>
			
			<div class="blog-summary">
				<?php //echo get_related_author_posts(); ?>

				<h5><a href="<?php get_permalink( $authors_post->ID ); ?>"><?php the_title(); ?></a></h5>
				<small><?php the_time('F jS, Y'); ?></small>
				<?php the_post_thumbnail(); ?>

				<?php the_excerpt(); ?>

<!-- 				<h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

				<small><?php the_time('F jS, Y'); ?> by <?php the_author_posts_link(); ?></small>

				<?php the_post_thumbnail(); ?>

				<?php the_excerpt(); ?>
				</div><!-- /.blog-summary -->

			<hr>

		<?php } ?>

		<?php wp_reset_query(); ?>
		
		<div class="clearfix"></div>

		</div>
	</div><!-- /.row -->
</section><!-- /.single-container -->

<?php get_footer(); ?>
